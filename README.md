# chatbot NSI

## Avant-propos

Ce document n'a pas la prétention de faire de vous des spécialistes des chatbots, mais seulement de vous donner les bases nécessaires afin de vous permettre de réaliser votre propre chatbot. J'ai donc essayé de simplifier les explications au maximum.

Au moment où j'écris ces lignes, je suis encore en pleine phase d'exploration. Par exemples, je travaille actuellement sur les agents, sur la possibilité d'utiliser un modèle multimodal  (pour intégrer notamment des images) et sur encore bien d'autres choses. Si je découvre des choses qui pourraient permettre d'améliorer ce chatbot, je republierai alors un article. Il ne faut donc pas voir ce texte comme quelque chose de figé, mais plutôt comme une première étape.

Il est possible de directement tester le chabot NSI : [https://assistantnsi.streamlit.app/](https://assistantnsi.streamlit.app/) 

## Introduction

Le développement des grands modèles de langage (LLM) a permis l'émergence d'agents conversationnels performants (par exemple ChatGPT, le chatbot d'OpenAI). Pourtant, ces agents conversationnels "généralistes" ont des défauts assez marqués : la nature statistique des technologies utilisées pour créer les LLM fait que les réponses apportées aux requêtes des utilisateurs sont souvent imprévisibles. Les modèles ont tendance à répondre n'importe quoi quand il ne trouve pas la réponse, on parle alors d'hallucinations du modèle.

La génération augmentée de récupération (RAG) est une technique qui permet d'atténuer ces problèmes.

## Présentation de La génération augmentée de récupération (RAG)

### choix des documents

L'idée qui se cache derrière la notion de RAG est relativement simple : le concepteur du chatbot va commencer par recenser un certain nombre de documents de "confiance" sur le sujet qui l'intéresse. Pour l'exemple qui nous intéresse ici (création d'un chatbot consacré à la spécialité NSI), j'ai utilisé les programmes officiels de NSI (première et terminale) au format Markdown, les ressources présentes sur mon [site perso](https://pixees.fr/informatiquelycee/) au format Markdown, les sujets de bac des années 2023 et 2024 au format PDF et enfin les sujets 2024 de l'épreuve pratique aussi au format PDF.  Ces documents vont servir de source principale d'informations pour le LLM.

### indexation

Il est important de comprendre qu'un LLM ne travaille pas directement sur des mots mais sur une représentation vectorielle de ces mots. Les documents choisis pour constituer la base du RAG vont donc être transformé en un grand nombre de vecteurs (en fait, les documents ne sont pas directement transformés en une suite de vecteurs, ils doivent être découpés en éléments plus petits, mais cet aspect des choses ne sera pas abordé ici). Cette représentation des mots en vecteurs se nomme l'embedding.

Une fois toutes ces transformations réalisées, tous ces vecteurs sont stockés dans un index, voilà pourquoi toute cette phase est appelée phase d'indexation.

### le prompt

Le prompt est une suite de phrases qui a pour but de donner des instructions au LLM afin d’orienter ses futures réponses aux requêtes de l’utilisateur.

Voici un extrait du prompt que j'utilise pour ce RAG NSI :

```
"Tu es un assistant spécialisé dans l'enseignement de la spécialité Numérique et sciences informatiques en classe de première et de terminal"
'Tu as un bon niveau en langage Python'
'Ton interlocuteur est un élève qui suit la spécialité nsi en première et en terminale'
'Tu dois uniquement répondre aux questions qui concernent la spécialité numérique et sciences informatiques'
"Tu ne dois pas faire d'erreur, répond à la question uniquement si tu es sûr de ta réponse"
"si tu ne trouves pas la réponse à une question, tu dois répondre que tu ne connais pas la réponse et que l'élève doit s'adresser à son professeur pour obtenir cette réponse"
"Tu dois uniquement aborder des notions qui sont aux programmes de la spécialité numérique et sciences informatiques (première et terminale), tu ne dois jamais aborder une notion qui n'est pas au programme"
```

Il existe des ouvrages entièrement consacrés à "l'art du prompting". Le travail sur le prompt est absolument fondamental, un mauvais prompt donnera, presque à coup sûr, un mauvais chatbot.

### un schéma pour comprendre

Le schéma ci-dessous devrait vous permettre de comprendre les processus mis en jeu :

![Schéma RAG](./schema_rag.png)

- L'utilisateur fait une requête
- Le système (plus exactement le retriever), va "choisir" dans l'index les documents les plus pertinents (ayant un rapport avec la requête)
- Le triplet prompt, requête et documents choisis par le retriever est envoyé au LLM. Le LLM peut alors répondre à la requête.

## Analyse de app.py

```
from dotenv import load_dotenv
import os
from llama_index.core import VectorStoreIndex, StorageContext, SimpleDirectoryReader, load_index_from_storage
from llama_index.embeddings.huggingface import HuggingFaceEmbedding
from llama_index.llms.groq import Groq
from llama_index.core import Settings
import streamlit as st
```

Nous allons principalement utiliser 2 frameworks pour réaliser notre chatbot :

- llamaindex qui est un framework qui facilite le développement d'applications basées sur les LLM
- streamlit qui va nous permettre d'avoir une interface web (au lieu d'avoir à travailler dans la console)

```
load_dotenv()
Settings.llm = Groq(temperature=0.8,model="llama3-70b-8192", api_key=os.getenv('GROQ_API_KEY'))
Settings.embed_model = HuggingFaceEmbedding(model_name="OrdalieTech/Solon-embeddings-large-0.1")
```

Nous allons utiliser 2 modèles de langage :
- un modèle classique (LLM) : "llama3-70b" (llama3, le modèle de méta dans sa version 70 milliard de paramètres)
- un modèle pour l'embedding : "Solon large de chez OrdalieTech" (modèle spécialisé dans l'embedding du français)

Le modèle "Solon" va tourner en local sur votre machine. Lors de la première exécution du code sur votre machine, le modèle va être téléchargé depuis la plateforme HuggingFace (cela peut prendre du temps, environ 2 Go à télécharger).

En revanche, il n'est pas possible d'utiliser le modèle llama3 70B sur une machine "classique" (si votre ordinateur possède une bonne carte graphique et environ 32 Go de RAM, la version 8B de llama3 devrait fonctionner). Nous allons donc utiliser les services de [GroqCloud](https://console.groq.com/playground). Cette société met à notre disposition une API qui permet de faire tourner un modèle sur leur serveur. Ce service est gratuit pour les particuliers qui sont prêts en faire quelques concessions (vitesse, nombre de tokens par minutes...). Pour utiliser les services de GroqCloud, vous allez devoir vous inscrire sur leur site afin d'obtenir une clé d'API (nous reviendrons sur ce point un peu plus loin).

```
try:
    storage_context = StorageContext.from_defaults(persist_dir = './storage')
    res_index = load_index_from_storage(storage_context)
    index_loaded = True
except:
    index_loaded = False

if not index_loaded:
    print('index creation, please wait...')
    res_doc = SimpleDirectoryReader('./documents').load_data()
    res_index = VectorStoreIndex.from_documents(res_doc)
    res_index.storage_context.persist(persist_dir = './storage')
    print('index creation completed')
```

Dans un premier temps, nous essayons de récupérer un éventuel index pré-existant dans le répertoire "storage". S'il n'est possible de récupérer un index, un nouvel index est construit à partir des documents se trouvant dans le répertoire "documents". Cet index fraichement construit est sauvegardé dans le répertoire "storage".

```
st.set_page_config(page_title="ASSISTANT NSI (première et terminale)", layout="centered", initial_sidebar_state="auto", menu_items=None)
st.title("Assistant NSI")
st.info("Cet assistant vous permet de réviser le cours et faire des exercices.")
```

Ici Streamlit rentre en jeu : rien de bien compliqué à comprendre

```
if "messages" not in st.session_state.keys(): 
    st.session_state.messages = [
        {"role": "assistant", "content": "Bonjour, que puis-je faire pour vous ?"}
    ]
if "chat_engine" not in st.session_state.keys(): 
    st.session_state.chat_engine = res_index.as_chat_engine(
    chat_mode="context",
    system_prompt = (
        "Tu es un assistant spécialisé dans l'enseignement de la spécialité Numérique et sciences informatiques en classe de première et de terminal"
        'Tu as un bon niveau en langage Python'
        'Tu dois commencer la conversation'
        "Inspire-toi des sujets de bac donnés en exemple pour créer des exercices"
        "Inspire-toi des sujets d'épreuve pratique pour créer des exercices sur la programmation en Python"
        'Ton interlocuteur est un élève qui suit la spécialité nsi en première et en terminale'
        'Tu dois uniquement répondre aux questions qui concernent la spécialité numérique et sciences informatiques'
        "Tu ne dois pas faire d'erreur, répond à la question uniquement si tu es sûr de ta réponse"
        "si tu ne trouves pas la réponse à une question, tu réponds que tu ne connais pas la réponse et que l'élève doit s'adresser à son professeur pour obtenir cette réponse"
        "Tu dois uniquement aborder des notions qui sont aux programmes de la spécialité numérique et sciences informatiques (première et terminale), tu ne dois jamais aborder une notion qui n'est pas au programme"
        'Tu dois uniquement répondre en langue française'
        'les réponses doivent être données au format Markdown'
    )
)
```

Si le système de chat n'a pas encore été mis en place (clé "chat_engine" dans le dictionnaire "st.session_state"), nous le créons. Vous avez sans doute déjà remarqué que c'est à ce niveau que nous définissons notre prompt.

```
prompt = st.chat_input("À vous...")
if prompt :
    st.session_state.messages.append({"role": "user", "content": prompt})
for message in st.session_state.messages: 
    with st.chat_message(message["role"]):
        st.write(message["content"])
if st.session_state.messages[-1]["role"] != "assistant":
    with st.chat_message("assistant"):
        with st.spinner("Je réfléchis..."):
            response = st.session_state.chat_engine.chat(prompt)
            st.write(response.response)
            message = {"role": "assistant", "content": response.response}
            st.session_state.messages.append(message)
```

Nous terminons par le chat à proprement parlé : il y a deux rôles qui alternent : "user" et "assistant". La conversation est enregistrée dans le dictionnaire st.session_state (clé "messages"). Il est très important de bien comprendre que l'historique de la conversation est envoyé au LLM à chaque requête de "user" (ce qui donne l'illusion d'une conversation).


## Comment utiliser ce chatbot sur sa machine ?

Après avoir cloné le dépôt "rag-nsi" sur votre ordinateur, il vous suffit de :
- récupérer une clé d'API depuis le site [GroqCloud](https://console.groq.com/playground)
- dans une console, depuis le répertoire "rag-nsi", taper : pip install -r requierement.txt
- dans le même répertoire, créer un fichier .env
- renseigner la clé API de groq dans le fichier .env : GROQ_API_KEY = "xxxxxxxxx" (remplacer les x par votre clé)
- toujours depuis le même répertoire, taper dans la console : streamlit run app.py

Cette dernière commande devrait lancer le serveur web, il vous reste donc à ouvrir votre navigateur web.

N.B : je vous rappelle qu'au cours de cette première exécution, le modèle d'embedding devrait se télécharger, il va donc falloir faire preuve de patience


## Comment créer son propre chatbot ?

- remplacer les fichiers présents dans le répertoire documents par vos propres fichiers (pdf, md, docx, html, ...)
- supprimer le répertoire storage
- modifier le prompt dans le fichier app.py (variable system_prompt)

N.B : la création du nouvel index va prendre un peu de temps...


